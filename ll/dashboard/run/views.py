# This file is part of Living Labs Challenge, see http://living-labs.net.
#
# Living Labs Challenge is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Living Labs Challenge is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Living Labs Challenge. If not, see <http://www.gnu.org/licenses/>.

from flask import Blueprint, request, render_template, flash, g, session, redirect, url_for
from .. import core, requires_login

mod = Blueprint('runs', __name__, url_prefix='/runs')


@mod.route('/')
@requires_login
def all():
    systems = core.system.get_systems(g.user['_id'])
    return render_template("run/all.html", user=g.user, systems=systems,
                           config=core.config.config)


@mod.route('/<run_id>')
@requires_login
def run(run_id):
    system = core.system.get_system(g.user['_id'], run_id)
    can_activate, criteria = core.system.activation_status(system['_id'])
    return render_template("run/run.html", system=system, user=g.user,
                           criteria=criteria, can_activate=can_activate,
                           config=core.config.config)


@mod.route('/deactivate/<run_id>')
@requires_login
def deactivate(run_id):
    system = core.system.get_system(g.user['_id'], run_id)
    core.system.set_activation(system['_id'], False)
    flash('Run %s has been deactivated.' % run_id, 'alert-success')
    return redirect(url_for('runs.run', run_id=run_id))


@mod.route('/activate/<run_id>')
@requires_login
def activate(run_id):
    system = core.system.get_system(g.user['_id'], run_id)
    if core.system.activation_status(system['_id'])[0]:
        core.system.set_activation(system['_id'], True)
        flash('Run %s has been activated.' % run_id, 'alert-success')
    else:
        flash('Run %s cannot be activated.' % run_id, 'alert-error')
    return redirect(url_for('runs.run', run_id=run_id))

