# This file is part of Living Labs Challenge, see http://living-labs.net.
#
# Living Labs Challenge is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Living Labs Challenge is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Living Labs Challenge. If not, see <http://www.gnu.org/licenses/>.

import slugify
from db import db
from config import config
import random
import pymongo
import datetime
import site
import user
import query
import feedback
import system


def get_ranking(site_id, site_qid):
    """
    Get a ranking produced by participants of the Challenge selected based on a
    least-served basis. This increments the display counter of the selected
    run.

    :param site_id: The site identifier
    :param site_qid: The query identifier for which to return a ranking
    :return: A participant's ranking for given query
    """
    q = db.query.find_one({"site_id": site_id, "site_qid": site_qid})
    if not q:
        raise LookupError("Query does not exist: site_qid = '%s'" % site_qid)

    # Select the next run to display (this selects a user's run of a user
    # who has received the least number of displays so far)
    sys = system.get_next_system_to_display(site_id)

    # Get the corresponding run of that run
    run = db.run.find({'runid': sys['runid'], 'site_qid': site_qid}).next()

    # Increment display counter for run
    system.increment_nr_times_displayed(sys['_id'])

    # Create a feedback session entry
    sid = site.next_sid(site_id)
    feedback = {
        "_id": sid,
        "site_qid": site_qid,
        "site_id": site_id,
        "qid": q["_id"],
        "runid": run["runid"],
        "userid": run["userid"],
        "creation_time": datetime.datetime.now(),
    }
    db.feedback.save(feedback)
    run['sid'] = sid

    # Return resulting doclist
    return run


def add_run(key, qid, runid, doclist):
    """
    Add a run for a particular query

    :param key: The participant's API key
    :param qid: The query identifier
    :param runid: The run identifier, used to identify different systems of a participant
    :param doclist: The document list for this query
    """
    q = db.query.find_one({"_id": qid})
    if not q:
        raise LookupError("Query does not exist: qid = '%s'" % qid)

    # If this runid is new, we need to create a "system" for it
    if system.get_system(key, runid) is None:
        system.add_system(key, runid, q['site_id'])

    # Check if we are in the test period
    in_test_period = False
    for test_period in config["TEST_PERIODS"]:
        if test_period["START"] < datetime.datetime.now() < test_period["END"]:
            in_test_period = True
            break

    # If we are in a test period and the query is a test query and this user
    # already submitted, we throw an error
    if in_test_period and "type" in q and q["type"] == "test" \
            and "runs" in q and key in q["runs"]:
        raise ValueError("For test queries you can only upload a run once "
                         "during a test period.")

    # Check if the site that the user is submitting to, is one that they are
    # signed up for
    sites = user.get_sites(key)
    if q["site_id"] not in sites:
        raise LookupError("First sign up for site %s." % q["site_id"])

    # If the query does not have documents or if the submitted ranking contains
    # a document that is not in the list of documents for this query, throw an
    # error
    if len(doclist) == 0:
        raise ValueError("The doclist should contain documents.")
    for doc in doclist:
        doc_found = db.doc.find_one({"_id": doc["docid"]})
        if not doc_found:
            raise LookupError("Document not found: docid = '%s'. Only submit "
                              "runs with existing documents." % doc["docid"])
        doc["site_docid"] = doc_found["site_docid"]

    # Create a run entry and save it (remove old runs when overwriting with the
    # same runid)
    creation_time = datetime.datetime.now()
    run = {
        'runid': runid,
        'doclist': doclist,
        'qid': qid,
        'site_id': q["site_id"],
        'site_qid': q["site_qid"],
        'userid': key,
        'creation_time': creation_time,
    }
    db.run.remove({"runid": runid, "qid": qid, "userid": key})
    db.run.save(run)

    # Store our run in the query object as well
    if "runs" in q:
        runs = q["runs"]
    else:
        runs = {}
    runs[key] = (runid, creation_time)
    q["runs"] = runs
    db.query.save(q)

    return run


def get_trec_run(runs, periodname, teamname):
    runname = slugify.slugify(unicode("%s %s" % (periodname, teamname)))
    trec = []
    for qid in sorted(runs.keys()):
        ndoc = len(runs[qid]["doclist"])
        for rank, d in enumerate(runs[qid]["doclist"]):
            trec.append("%s Q0 %s %d %d %s" % (qid, d["docid"], rank,
                                               ndoc - rank, runname))
    return {"trec": "\n".join(trec),
            "name": runname}


def get_trec_qrel(feedbacks, periodname, rawcount=False):
    periodname = slugify.slugify(unicode(periodname))
    trec = []

    for qid in sorted(feedbacks.keys()):
        click_stat = {}
        count = 0
        for feedback in feedbacks[qid]:
            for d in feedback["doclist"]:
                if not d["docid"] in click_stat:
                    click_stat[d["docid"]] = [0, 0]
                if "clicked" in d and (d["clicked"] is True or
                                           (isinstance(d["clicked"], list) and
                                                    len(d["clicked"]) > 0)):
                    click_stat[d["docid"]][0] += 1
                click_stat[d["docid"]][1] += 1
            count += 1
        ctrs = []
        for d in click_stat:
            if rawcount:
                ctrs.append((click_stat[d][0], d))
            else:
                ctrs.append((float(click_stat[d][0]) / count, d))
        for ctr, d in sorted(ctrs, reverse=True):
            if rawcount:
                trec.append("%s 0 %s %d" % (qid, d, ctr))
            else:
                trec.append("%s 0 %s %.6f" % (qid, d, ctr))

    return {"trec": "\n".join(trec),
            "name": periodname}


def get_trec(site_id):
    trec_runs = []
    trec_qrels = []
    trec_qrels_raw = []
    queries = query.get_query(site_id)
    participants = user.get_participants()
    for test_period in config["TEST_PERIODS"]:
        if datetime.datetime.now() < test_period["END"]:
            continue
        for participant in participants:
            userid = participant["_id"]
            participant_runs = {}
            for q in queries:
                if "type" not in q or not q["type"] == "test":
                    continue
                qid = q["_id"]
                runs = db.run.find({"userid": userid,
                                    "qid": qid})
                if not runs:
                    continue
                testrun = None
                testrundate = datetime.datetime(2000, 1, 1)
                for run in runs:
                    if testrundate < run["creation_time"] < test_period["END"]:
                        testrundate = run["creation_time"]
                        testrun = run
                if not testrun:
                    continue
                participant_runs[qid] = testrun

            if participant_runs:
                trec_runs.append(get_trec_run(participant_runs,
                                              test_period["NAME"],
                                              participant["teamname"]))
        test_period_feedbacks = {}
        for q in queries:
            if "type" not in q or not q["type"] == "test":
                continue
            qid = q["_id"]
            feedbacks = feedback.get_test_feedback(site_id=site_id, qid=qid)
            test_period_feedbacks[qid] = [f for f in feedbacks if
                                          (test_period["START"] <
                                           f["creation_time"] <
                                           test_period["END"])]
        trec_qrels.append(get_trec_qrel(test_period_feedbacks,
                                        test_period["NAME"]))
        trec_qrels_raw.append(get_trec_qrel(test_period_feedbacks,
                                            test_period["NAME"],
                                            rawcount=True))
    return trec_runs, trec_qrels, trec_qrels_raw
