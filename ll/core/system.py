# This file is part of Living Labs Challenge, see http://living-labs.net.
#
# Living Labs Challenge is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Living Labs Challenge is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Living Labs Challenge. If not, see <http://www.gnu.org/licenses/>.

from db import db


def add_system(user_id, run_id, site_id, active=False):
    """
    Adds a new system for given user and run

    :param user_id: The user who owns the system
    :param run_id: The run-id associated with this system
    :param site_id: The site identifier for this system
    :param active: Whether this system is active
    :return:
    """
    return db.systems.insert({
        'userid': user_id,        # The user-id of the user who owns this system
        'runid': run_id,          # The run-id associated with this system
        'nr_times_displayed': 0,  # Number of times this system's rankings have
                                  # been displayed
        'active': active,         # Whether this system is active and can be
                                  # selected
        'siteid': site_id         # The site identifier for this system
    })


def get_system(user_id, run_id):
    """
    Checks if a system for given user_id and run_id already exists

    :param user_id: The user id
    :param run_id: The run id
    :return: The system object if it exists, None otherwise
    """
    return db.systems.find_one({'userid': user_id, 'runid': run_id})


def get_systems(user_id):
    """
    Returns all systems of a given user

    :param user_id: The user identifier
    :return: A list of system objects
    """
    return list(db.systems.find({'userid': user_id}))


def get_user_with_least_nr_times_displayed(site_id):
    """
    Gets the user identifier for the user that currently has the least number
    of displayed rankings

    :return: The user id as a string
    """
    cursor = db.systems.aggregate([
        {'$match': {'active': True, 'siteid': site_id}},
        {'$group': {'_id': '$userid', 'total': {'$sum': '$nr_times_displayed'}}},
        {'$sort': {'total': 1}},
        {'$limit': 1}
    ])
    if not cursor.alive:
        raise LookupError("Could not find a ranking")
    result = cursor.next()
    return result['_id']


def get_next_system_to_display(site_id):
    """
    Returns the next system to display a ranking for (selects by least-shown
    first)

    :return: The system as a json object
    """
    user_id = get_user_with_least_nr_times_displayed(site_id)
    return db.systems.find({
        'active': True, 'siteid': site_id, 'userid': user_id
    }).sort('nr_times_displayed', 1).next()


def increment_nr_times_displayed(system_id):
    """
    Increments the number of times a particular system has been displayed

    :param system_id: The system identifier (should be a bson.ObjectId)
    """
    db.systems.update_one({'_id': system_id},
                          {'$inc': {'nr_times_displayed': 1}})


def activation_status(system_id):
    """
    Returns the activation status of a system as a tuple containing a boolean
    stating whether this system can be activated and a string representing why
    it can't be activated

    :param system_id: The system identifier
    :return: A tuple containing as first element a boolean and second element
    the reason for the activation status as a string
    """
    sys = db.systems.find_one({'_id': system_id})
    site_id = sys['siteid']

    # If the system is already activated, just say so
    if sys['active']:
        return False, "This run is currently active and is used for impressions"

    # Check if the owner of the system already has x systems active:
    max_nr_of_systems = 5
    if db.systems.find({'userid': sys['userid'],
                        'active': True}).count() >= max_nr_of_systems:
        return False, "You already have %d active runs for this site. You " \
                      "need to disable an existing run before you can " \
                      "activate this run." % max_nr_of_systems

    # For every query in the site, check if a corresponding run with given
    # the runid of given system exists
    list_of_missing_queries = []
    max_nr_missing_queries_to_display = 4
    for q in db.query.find({'site_id': site_id, 'deleted': False}):
        current = {
            'userid': sys['userid'],
            'runid': sys['runid'],
            'site_qid': q['site_qid'],
            'site_id': sys['siteid']
        }
        if db.run.find(current).limit(1).count() == 0:
            list_of_missing_queries.append('"{qid}"'.format(qid=q['_id']))
            if len(list_of_missing_queries) > max_nr_missing_queries_to_display:
                break

    # Display list of missing queries
    if len(list_of_missing_queries) > 0:
        identifiers = ", ".join(list_of_missing_queries)
        if len(list_of_missing_queries) == max_nr_missing_queries_to_display:
            identifiers += ", ..."
        return False, "You are missing the rankings for some queries. You " \
                      "need to submit a ranking for every query before your " \
                      "run can be activated. You currently still need to " \
                      "submit rankings for queries %s" % identifiers

    return True, "This run can be activated."


def set_activation(system_id, active):
    """
    Activates or deactivates a system

    :param user_id: The user id
    :param run_id: The run id
    :param active: Whether to activate the system or not
    """
    db.systems.update_one({'_id': system_id}, {'$set': {'active': active}})
