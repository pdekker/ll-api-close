# This file is part of Living Labs Challenge, see http://living-labs.net.
#
# Living Labs Challenge is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Living Labs Challenge is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Living Labs Challenge. If not, see <http://www.gnu.org/licenses/>.

from flask import request
from flask_restful import Resource, fields, marshal
from .. import api
from .. import core
from .. import ApiResource
from .. import requires_auth

doc_fields = {
    "docid": fields.String(),
}

run_fields = {
    "creation_time": fields.DateTime(),
    "qid": fields.String(),
    "runid": fields.String(),
    "doclist": fields.Nested(doc_fields)
}


class Run(ApiResource):
    @requires_auth
    def put(self, key, qid):
        """
        Submit a run (ranking) for a specific query. It is possible to evaluate 
        multiple ranking algorithms (called systems) per participant. Different
        systems per participant are identified by their runid. You can use an
        arbitrary string as runid.
        Traffic per query is evenly spread across participants, so if you upload
        runs for multiple systems, you will receive less feedback per run.
        
        A run should contain a ranked list of all documents. It is not allowed
        to submit an empty document list.
        
        Submitting a run for a query with the same runid twice, will overwrite
        and save only the most recent one. You can view and activate your runs
        in the dashboard after submitting them via the API. Activating a run is
        required before your runs show up on the website, so do not forget to
        do that.

        For test queries, a run can only be uploaded outside of test periods.
        An exception to this rule is if you have never uploaded a run for a
        test query. Then, it can be uploaded once during a test period. This
        is to allow participants to join at any moment.
        This rule also entails that it is not possible to upload runs for
        multiple systems per test query during test periods.
        See test periods here: http://living-labs.net/challenge/.


        :param qid: the query identifier
        :status 200: valid key
        :status 403: invalid key
        :status 409: for test queries, runs can only be uploaded once.


        :reqheader Content-Type: application/json
        :content:
            .. sourcecode:: javascript

                {
                    "qid": "U-q22",
                    "runid": "82",
                    "doclist": [
                        {
                            "docid": "U-d4"
                        },
                        {
                            "docid": "U-d2"
                        }, ...
                    ],
                }

        """
        self.validate_participant(key)
        run = request.get_json(force=True)
        self.check_fields(run, ["doclist", "runid"])
        run = self.trycall(core.run.add_run, key, qid, run["runid"], run["doclist"])
        return marshal(run, run_fields)

api.add_resource(Run, '/api/v2/participant/run/<qid>',
                 endpoint="participant/run")
