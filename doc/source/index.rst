.. ll-challenge documentation master file, created by
   sphinx-quickstart on Fri Apr 25 22:11:32 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

TREC OpenSearch Documentation
=============================

.. toctree::
   :maxdepth: 3
   :numbered:
   
   guide-participant
   api
   local-api-installation
   developer-information
