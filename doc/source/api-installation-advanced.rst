
Advanced options
================
Congratulations! You are done setting up a LivingLabs API including database, dashboard, sites and clients. Now, we will show some more advanced options to customize your environment.

Create users
------------
If there is a :ref:`Dashboard <dashboard>` running, you can create participants
by choosing `Register` on the :ref:`Dashboard <dashboard>`. It is also
possible to create users via the command line, this also enables you
to create site and admin users.

To create an example participant and a site (for development/testing purposes),
you can run the following script (without :code:`-c config/db.ini` for an unauthenticated setup):

.. sourcecode:: bash 

    $ ./bin/admin user -c config/db.ini config/example-data/site.ini --password CHOOSEAPASSWORD
    $ ./bin/admin user -c config/db.ini config/example-data/user.1.ini --password CHOOSEAPASSWORD

The passwords are used for the `Dasboard`.

In return, you will see two API keys, one for a site and one for a participant.
Record the keys as SITEKEY and PARTICIPANTKEY, you'll need them for the clients.

Instead, you can also provide your own details, or perform actions like deleting users and making users admin. See the help on how to do that:

.. sourcecode:: bash 

   $ ./bin/admin user -h

Do not forget to supply the configuration file as an argument, this gives the API the credentials to log in to the MongoDB database.

Export the database
-------------------
You can export the database to create a human-readable json fixture, like the one we use to :ref:`fill the database<fill_db>`.
To create a fixture in the `dump` directory, issue (without :code:`-c config/db.ini` for an unauthenticated setup):

.. sourcecode:: bash 

   $ ./bin/admin db --export-json dump -c config/db.ini


Reset the Database
------------------

In case you need a reset, you can simply run this (without :code:`-c config/db.ini` for an unauthenticated setup). But, BE CAREFUL, it can not
be undone (or, probably it can, the MongoDB is journalled, but it will not be
trivial).

.. sourcecode:: bash 

   $ ./bin/admin db --clear -c config/db.ini

Do not forget to recreate users (see above).


Remove all runs for a participant
---------------------------------

There can be cases where you may want to remove all runs for a certain participant.
For example, if a participant has a ranking algorithm which is not competitive,
it will only take user impressions from the other participants. The participant
could then request you as administrator to remove the runs.

The admin tool has a :code:`--remove-runs` option to accomplish this. Supply the key
of the participant for which you want to remove all runs. (without :code:`-c config/db.ini` for an unauthenticated setup)

 .. danger::
    All runs for the given participant will be removed from the :code:`query`
    collection! Sites will thus not be able to find rankings from this participant
    when looking for a query. The runs are still available in the :code:`run`
    collection, but they are not easy to look up from there.

.. sourcecode:: bash

   $ ./bin/admin user --remove-runs -c config/db.ini --key KEY
